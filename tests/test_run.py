import unittest
from smear.runner import run_smearing


class TestRunFile(unittest.TestCase):

    def test_run_beagle(self):
        run_smearing()


    def test_skip_parsing(self):
        """Must skip 120 events and process everything else"""
        result = range_to_ejana_params('120-')
        self.assertEqual(result['nskip'], 120)
        self.assertNotIn('nevents', result.keys())

    def test_range_parsing(self):
        """Must skip 120 events and process everything else"""
        result = range_to_ejana_params('120-200')
        self.assertEqual(result['nskip'], 120)
        self.assertEqual(result['nevents'], 80)


if __name__ == '__main__':
    unittest.main()
