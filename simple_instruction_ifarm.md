# Fast simulations for the EIC

We show how to run fast simulations from the command line. 
In this instructions, we assume that you work on JLab ifarm. 
Alternatively there are:
 
 - [JLab JupyterHub](https://gitlab.com/eic/escalate/workspace/-/blob/master/RemoteWork.md) (and skip the first two steps below)  
 - [Docker approach](simple_instruction_v1.md),
 - [run a singularity container]() on the BNL and JLAB systems. 

## 1. Setup environment

login to any JLab machine that have an access to /group disk 

```bash
source /group/eic/escalate/env.sh   # use env.csh for tcsh
```

> It is recommended to start with semi clean environemnt. 
> If some previous environment variables such as ROOTSYS are set, there might cases when it led to bugs 

## 2. Smear a generator file

### Smear a file 

To smear a generator file called ```/some/path/my_file.dat``` the command is:

```sh
smear /some/path/my_file.dat
```
For tests, you can use one of the files located in ```/group/eic/mc``` directory

For more details, please see our [full documentation on the smear command](https://gitlab.com/eic/escalate/smear) 
or run ```smear --help```


### Select a detector

By default the latest Handbook detector from eic-smear package is used. To change the detector use ```-d <name>```flag: 
```sh
smear -d jleic my_file.dat
```
To see all detectors and versions use ```-l```flag. 

### Specify number of events
One can set a number of events by ```-n(--nevents)``` and ```-s(--nskip)``` flags. Flag ```-n``` also support ranges:
```  
-n 100        : process 100 events
-s 100 -n 50  : skip first 100 events, and process 50 events
-s 100        : skip first 100 events and process the rest
```
### Available detector names and versions
| Engine | Name         | Version and Link |
|--------|--------------|------------------|
| ES     | handbook     | [HandBook v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorHandBook_v1_0_4.cc) |
| ES     | beast        | [BeAST v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorBeAST_v1_0_4.cc) |
| ES     | ephenix      | [ePHENIX v1.0.4](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorEPHENIX_v1_0_4.cc) |
| ES     | zeus         | [DetectorZeus v1.0.0](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/ESDetectorZeus_v1_0_0.cc) |
| YF     | yfhandbook   | [Handbook v1.0.0](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorHandbook_v1_0_0.cc) |
| YF     | jleic        | [Jleic v1.0.2](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorJleic_v1_0_2.cc) |
| YF     | jleic-v1.0.1 | [Jleic v1.0.1](https://gitlab.com/eic/escalate/ejana/blob/master/src/plugins/reco/eic_smear/YFDetectorJleic_v1_0_1.cc) |
> Comment: This section needs to be improved with better sames for the smearing engines, currently listed as ES - eic-smear and YF - Yulia Furletova, and the detecot names. 


